# pixmagic

Make the Web (using images) light again

## Prerequisites

- [pixresize.sh](pixresize.sh) requires ImageMagick v7+

If you don't have this version, [compile and install it](https://humanize.me/nerd/imagemagick.html) (guide).

## Install

```sh
sudo wget https://codeberg.org/jrm-omg/pixmagic/raw/branch/main/pixresize.sh -O /usr/local/bin/pixresize.sh; \
sudo chmod +x /usr/local/bin/pixresize.sh;
```

Note : the above lines can be used to update your `pixresize.sh` script as well.

## About pixresize.sh parameters

Pixresize is using the following parameters :

```sh
magick <INPUT> -auto-orient -strip -quality 82 -resize 1280x1280\> -unsharp 0.25x0.08+8.3+0.045 -interlace Plane -colorspace sRGB <OUTPUT>;
```

Parameters :

- `auto-orient` automatically orient image
- `strip` strip image of all profiles and comments
- `quality` compression level
- `resize` resize the image (the `>`, escaped with `\>`, resize only when input is larger than output)
  > Note : I've tried also using the `thumbnail` parameter : it makes lighter thumbnails, but makes the thumbnail less sharpen. I've noticed that the same "less sharpen" results can be observed using the `resize` parameter with `-define filter:support=2`.
- `unsharp` sharpen the image
- `interlace` interlacing scheme (`Plane` makes the JPG loading progressively)
- `colorspace` image colorspace

For all parameters, see the [ImageMagick documentation](https://imagemagick.org/script/convert.php).

### Batch resizing

```sh
mkdir thumbs; mogrify -path thumbs -auto-orient -strip -quality 82 -resize 1280x1280\> -unsharp 0.25x0.08+8.3+0.045 -interlace Plane -colorspace sRGB *.jpg
```

## More readings

Most of the parameters are based on the following article : [Dave Newton - Efficient Image Resizing With ImageMagick](https://www.smashingmagazine.com/2015/06/efficient-image-resizing-with-imagemagick/).
