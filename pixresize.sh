#!/bin/bash

# install or update
# sudo wget https://codeberg.org/jrm-omg/pixmagic/raw/branch/main/pixresize.sh -O /usr/local/bin/pixresize.sh; sudo chmod +x /usr/local/bin/pixresize.sh;

if [ -z "${1}" ]; then
    echo "pixresize.sh | AGPL Licence | https://codeberg.org/jrm-omg"
    echo ""
    echo "USAGE"
    echo "pixresize.sh <input> (<maxWidth x maxHeight>) (quality)"
    echo ""
    echo "EXAMPLES"
    echo "pixresize.sh input.jpg"
    echo "pixresize.sh input.jpg 1280x1280"
    echo "pixresize.sh input.jpg 1280x1280 70"
    echo "find -iname \"*.jpg\" -exec pixresize.sh {} 1280x1280 \;"
    exit
fi

# ImageMagick 7+ checkpoint
version=$(identify --version|sed "-e s/Version: ImageMagick //" -e "s/ .*//"|head -1)
if [ $version \< 7 ]; then
    echo "this script requires ImageMagick 7+"
    exit
fi

# "find" command line detection
first_chars=$(echo "${1}" | cut -c -2);
if [ $first_chars = "./" ]; then
  input=$(echo "${1}" | cut -c 3-);
else
  input=$1;
fi

# Input
input=$1
input_basename="${input##*/}"
input_path=$(dirname "${input}")
input_filename=$(basename "${input%.*}")
input_extension="${input_basename##*.}"
input_extension=${input_extension,,} # lowercase

# Dimensions
if [[ -z "${2}" ]]; then
    r=""
else
    r="-resize ${2}\>"
fi

# Quality
if [[ -z "${3}" ]]; then
    q=83
else
    q=$3
fi

# Process
tmp=$(cat /dev/urandom | tr -dc 'a-zA-Z0-9' | fold -w 32 | head -n 1)".${input_extension}"
magick "${input}" -auto-orient -strip -quality ${q} ${r} -unsharp 0.25x0.08+8.3+0.045 -interlace Plane -colorspace sRGB ${tmp};
s=$(identify -format '%wx%h' ${tmp})
out=${input_filename}-${s}-q${q}.${input_extension}
mv "${tmp}" "${out}"
length_in=$(du -h "${input}" | cut -f1)
length_out=$(du -h "${out}" | cut -f1)
echo "${input} (${length_in}) → ${out} (${length_out})"